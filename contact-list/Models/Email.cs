﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace contact_list.Models
{
    public class Email
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        public int ContactId { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(ContactId))]
        public virtual Contact Contact { get; set; }


    }
}
