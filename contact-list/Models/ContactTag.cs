﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace contact_list.Models
{
    public class ContactTag
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int ContactId { get; set; }

        [Required]
        public int TagId { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(ContactId))]
        public virtual Contact Contact { get; set; }
        
        [ForeignKey(nameof(TagId))]
        public virtual Tag Tag { get; set; }


    }
}
