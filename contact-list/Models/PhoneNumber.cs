﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace contact_list.Models
{
    public class PhoneNumber
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Phone]
        public string Number { get; set; }

        [Required]
        public int ContactId { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(ContactId))]
        public virtual Contact Contact { get; set; }
    }
}
