﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using contact_list.Models;
using contact_list.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace contact_list.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private readonly IContactsService _contactsService;
        public ContactsController(IContactsService contactsService)
        {
            _contactsService = contactsService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var contacts = _contactsService.GetAllContacts();
            return Ok(contacts);
        }

        [HttpGet("name/{name}")]
        public ActionResult<IEnumerable<string>> GetByName(string name)
        {
            var contacts = _contactsService.getContactByName(name);
            return Ok(contacts);
        }

        [HttpGet("surname/{surname}")]
        public ActionResult<IEnumerable<string>> GetBySurname(string surname)
        {
            var contacts = _contactsService.getContactBySurname(surname);
            return Ok(contacts);
        }

        [HttpGet("tag/{tagid}")]
        public ActionResult<IEnumerable<string>> GetByTag(int tagId)
        {
            var contacts = _contactsService.getContactByTag(tagId);
            return Ok(contacts);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Contact>> Get(int id)
        {
            try
            {
                var contact = await _contactsService.GetContactByIdAsync(id);
                return Ok(contact);
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpPost(nameof(Create))]
        public async Task<ActionResult> Create([FromBody] Contact contact)
        {
            try
            {
                await _contactsService.AddContactAsync(contact);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            
        }

        [HttpPut("update/{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] Contact contact)
        {
            try
            {
                await _contactsService.UpdateContactAsync(id, contact);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete (int id)
        {
            try
            {
                await _contactsService.DeleteContactAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

      
        [HttpPost("{contactId}/{tagId}")]
        public async Task<ActionResult> Connect(int contactId, int tagId)
        {
            try
            {
                await _contactsService.AddTagToContact(contactId, tagId);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}