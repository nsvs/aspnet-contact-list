﻿using contact_list.Models;
using contact_list.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace contact_list.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagsController : ControllerBase
    {
        private readonly ITagsService _tagsService;
        public TagsController(ITagsService tagsService)
        {
            _tagsService = tagsService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            //IEnumerable<Tag> tags = null;
            try
            {
                var tags = _tagsService.GetAllTags();
                return Ok(tags);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPost(nameof(Create))]
        public async Task<ActionResult> Create([FromBody] Tag tag)
        {
            try
            {
                await _tagsService.AddTagAsync(tag);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPut("update/{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] Tag tag)
        {
            try
            {
                await _tagsService.UpdateTagAsync(id, tag);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _tagsService.DeleteTagAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
