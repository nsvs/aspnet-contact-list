﻿using contact_list.DAL;
using contact_list.Models;
using contact_list.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace contact_list.Services
{
    public class ContactsService : IContactsService
    {
        private readonly DBContext _dBContext;
        public ContactsService(DBContext context)
        {
            _dBContext = context;
        }
        public IEnumerable<Contact> GetAllContacts()
        {
            var contacts = _dBContext.Contacts.Include("Emails")
                                              .Include("PhoneNumbers")
                                              .Include("ContactTags")
                                              .Include("ContactTags.Tag");


            return contacts.ToList();
        }

        public async Task<Contact> GetContactByIdAsync(int id)
        {
            var targetContact = await _dBContext.Contacts.FindAsync(id);
            if (targetContact == null)
            {
                throw new Exception("Contact does not exist!");
            }
            targetContact.Emails = _dBContext.Emails.Where(x => x.ContactId == id).ToList();
            targetContact.PhoneNumbers = _dBContext.PhoneNumbers.Where(x => x.ContactId == id).ToList();
            targetContact.ContactTags = _dBContext.ContactTags.Include("Tag").Where(x => x.ContactId == id).ToList();

            return targetContact;
        }

        public async Task AddContactAsync(Contact contact)
        {
            var targetContact = await _dBContext.Contacts.AddAsync(contact);
            await _dBContext.SaveChangesAsync();
        } 
        public async Task UpdateContactAsync(int id, Contact contact)
        {
            var targetContact = await _dBContext.Contacts.FindAsync(id);
            if (targetContact == null)
            {
                throw new Exception("Contact does not exist!");
            }

            ClearUserData(id);

            targetContact.Name = contact.Name;
            targetContact.Surname = contact.Surname;
            targetContact.Address = contact.Address;
            targetContact.IsFavourite = contact.IsFavourite;
            targetContact.Emails = contact.Emails;
            targetContact.PhoneNumbers = contact.PhoneNumbers;
            targetContact.ContactTags = contact.ContactTags;

            _dBContext.Contacts.Update(targetContact);
            await _dBContext.SaveChangesAsync();            
        }

        public async Task DeleteContactAsync(int id)
        {
            var targetContact = await _dBContext.Contacts.FindAsync(id);

            if (targetContact == null)
            {
                throw new Exception("Contact does not exist!");
            }

            ClearUserData(id);

            _dBContext.Contacts.Remove(targetContact);
            await _dBContext.SaveChangesAsync();

        }

        public IEnumerable<Contact> getContactByName(string name)
        {
            // polovicna pretraga na backendu .. na frontendu ce se filtrirat one s 
            // razlicitim IDevima
            var contacts = _dBContext.Contacts.Where(x => x.Name == name).ToList();
            return contacts;
        }

        public IEnumerable<Contact> getContactBySurname(string surname)
        {
            var contacts = _dBContext.Contacts.Where(x => x.Surname == surname).ToList();
            return contacts;
        }

        public IEnumerable<Contact> getContactByTag(int tagId)
        {
            var contacts = _dBContext.Contacts
                .Include("ContactTags")
                .Include("ContactTags.Tag")
                .Where(x => x.ContactTags.Any(y => y.TagId == tagId));

            return contacts;
        }

        // n - n
        public async Task  AddTagToContact(int contactId, int tagId)
        {
            var targetContact = await _dBContext.Contacts.FindAsync(contactId);
            var targetTag = await _dBContext.Tags.FindAsync(tagId);
            if (targetContact == null || targetTag == null)
            {
                throw new Exception("Ex");
            }

            var targetInsert = new ContactTag() { ContactId = contactId, TagId = tagId };

            await _dBContext.ContactTags.AddAsync(targetInsert);
            await _dBContext.SaveChangesAsync();
        }

        private void ClearUserData(int id)
        {
            var emailToRemove = _dBContext.Emails.Where(x => x.ContactId == id).ToList();
            _dBContext.Emails.RemoveRange(emailToRemove);

            var numbersToRemove = _dBContext.PhoneNumbers.Where(x => x.ContactId == id).ToList();
            _dBContext.PhoneNumbers.RemoveRange(numbersToRemove);

            var tagsToRemove = _dBContext.ContactTags.Where(x => x.ContactId == id).ToList();
            _dBContext.ContactTags.RemoveRange(tagsToRemove);

            _dBContext.SaveChanges(); 
        }
    }
}
