﻿using contact_list.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace contact_list.Services.Interfaces
{
    public interface ITagsService
    {
        IEnumerable<Tag> GetAllTags();
        Task AddTagAsync(Tag tag);
        Task UpdateTagAsync(int id, Tag tag);
        Task DeleteTagAsync(int id);
    }
}
