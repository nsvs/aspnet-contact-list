﻿using contact_list.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace contact_list.Services.Interfaces
{
    public interface IContactsService
    {
        IEnumerable<Contact> GetAllContacts();
        Task<Contact> GetContactByIdAsync(int id);
        Task AddContactAsync(Contact contact);
        Task UpdateContactAsync(int id, Contact contact);
        Task DeleteContactAsync(int id);
        IEnumerable<Contact> getContactByName(string name);
        IEnumerable<Contact> getContactBySurname(string surname);
        IEnumerable<Contact> getContactByTag(int tagId);
        Task AddTagToContact(int contactId, int tagId);
    }
}
