﻿using contact_list.DAL;
using contact_list.Models;
using contact_list.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace contact_list.Services
{
    public class TagsService : ITagsService
    {
        private readonly DBContext _dBContext;
        public TagsService(DBContext context)
        {
            _dBContext = context;
        }

        public IEnumerable<Tag> GetAllTags()
        {
            var tags = _dBContext.Tags;
            return tags.ToList();
        }

        public async Task AddTagAsync(Tag tag)
        {
            await _dBContext.Tags.AddAsync(tag);
            await _dBContext.SaveChangesAsync();
        }

        public async Task UpdateTagAsync (int id, Tag tag)
        {
            var targetTag = await _dBContext.Tags.FindAsync(id);

            if (targetTag == null)
            {
                throw new Exception("Tag does not exist!");
            }

            targetTag.Name = tag.Name;

            _dBContext.Tags.Update(targetTag);
            await _dBContext.SaveChangesAsync();
        }

        public async Task DeleteTagAsync (int id)
        {
            var targetTag = await _dBContext.Tags.FindAsync(id);

            if (targetTag == null)
            {
                throw new Exception("Tag does not exist!");
            }

            _dBContext.Tags.Remove(targetTag);
            await _dBContext.SaveChangesAsync();
        }
    }
}
